module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      all: {
        files: ['www/css/*.less', '!www/css/*.css'],
        tasks: ['less'],
      }
    },

    less: {
      development: {
        files: {
          'www/css/index.css': 'www/css/index.less'
        }
      },
    },

  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  grunt.registerTask('default', ['less', 'watch']);
};