# Phonegap MYAPP

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Bower](http://bower.io): Run `[sudo] npm install -g bower`

## Quickstart

```bash
npm install
bower install
```

While you're working on your project, run:
```bash
grunt
```

And to deploy it to a device, run:
```bash
./run.sh
```

And you're set!